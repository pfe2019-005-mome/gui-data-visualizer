import * as THREE from './three.module.js';

import Stats from './stats.module.js';
import {GUI} from './dat.gui.module.js';

import {GLTFLoader} from './GLTFLoader.js';

import {OrbitControls} from './OrbitControls.js';

export class SceneVisualisation {

    constructor() {
        this.modelbones = [];
    }

    init() {

        let container = document.getElementById('model3D');

        this.camera = new THREE.PerspectiveCamera(45, (window.innerWidth/2) / window.innerHeight, 1, 1000);
        this.camera.position.set(1, 3, -5);
        this.camera.lookAt(0, 1, 0);

        this.clock = new THREE.Clock();

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xa0a0a0);
        this.scene.fog = new THREE.Fog(0xa0a0a0, 10, 50);

        let hemiLight = new THREE.HemisphereLight(0xffffff, 0x444444);
        hemiLight.position.set(0, 20, 0);
        this.scene.add(hemiLight);

        let dirLight = new THREE.DirectionalLight(0xffffff);
        dirLight.position.set(-3, 10, -10);
        dirLight.castShadow = true;
        dirLight.shadow.camera.top = 2;
        dirLight.shadow.camera.bottom = -2;
        dirLight.shadow.camera.left = -2;
        dirLight.shadow.camera.right = 2;
        dirLight.shadow.camera.near = 0.1;
        dirLight.shadow.camera.far = 40;
        this.scene.add(dirLight);


        let mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(100, 100), new THREE.MeshPhongMaterial({
            color: 0x999999,
            depthWrite: false
        }));
        mesh.rotation.x = -Math.PI / 2;
        mesh.receiveShadow = true;
        this.scene.add(mesh);

        let loader = new GLTFLoader();
        loader.load('models/gltf/Soldier.glb', function (gltf) {

            this.model = gltf.scene;
            this.scene.add(this.model);

            this.model.traverse(function (object) {
                if (object.isBone) {
                    console.log(object.name);
                    this.modelbones[object.name] = object;
                }
                if (object.isMesh) object.castShadow = true;

            }.bind(this));

            this.skeleton = new THREE.SkeletonHelper(this.model);
            this.skeleton.visible = false;
            this.scene.add(this.skeleton);

            this.createPanel();

            this.mixer = new THREE.AnimationMixer(this.model);

            this.renderer.render(this.scene, this.camera);

        }.bind(this));

        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth/2, window.innerHeight );
        this.renderer.gammaOutput = true;
        this.renderer.gammaFactor = 2.2;
        this.renderer.shadowMap.enabled = true;
        container.appendChild(this.renderer.domElement);

        this.stats = new Stats();
        container.appendChild(this.stats.dom);

        window.addEventListener('resize', this.onWindowResize, false);

        let orbit = new OrbitControls(this.camera, this.renderer.domElement);
        orbit.enableZoom = true;



    }

    createPanel() {

        let panel = new GUI({width: 310});

        let folder1 = panel.addFolder('Visibility');

        let settings = {
            'show model': true,
            'show skeleton': false,
        };

        folder1.add(settings, 'show model').onChange(this.showModel.bind(this));
        folder1.add(settings, 'show skeleton').onChange(this.showSkeleton.bind(this));

        folder1.open();

    }


    showModel(visibility) {
        this.model.visible = visibility;
    }

    showSkeleton(visibility) {
        this.skeleton.visible = visibility;
    }

    onWindowResize() {
        if (this.camera != undefined) {
            this.camera.aspect = (window.innerWidth/2) / window.innerHeight;
            this.camera.updateProjectionMatrix();
        }
        if (this.renderer != undefined) {
            this.renderer.setSize(window.innerWidth/2, window.innerHeight);
        }
    }

}