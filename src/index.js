import * as THREE from './3D/three.module.js';
import {SceneVisualisation} from './3D/sceneVisualisation.js'
import {SensorData} from './models/sensor-data.js';
import {Motion, MotionIterator, FullMotion} from './models/motion.js';
import io from 'socket.io-client';

let requestAnimationFrameId = 0;
let sceneVisualisation = new SceneVisualisation();
let recording = false;
let motioniterators = {};

/*
data.csv
x,y,z accelro
x,y,z accelro sans grav
x,y,z gyro

function getData() {
    let values = [];
    let loader = new THREE.FileLoader();
    loader.load("data.csv",
        function (data) {
            let tmpvalues = data.split(" ").map(function (i) {
                return parseFloat(i);
            });

            for (i = 6; i < tmpvalues.length; i += 9) {
                let elem = {"x": tmpvalues[i], "y": tmpvalues[i + 1], "z": tmpvalues[i + 2]};
                values.push(elem);
            }

            for (let i = 0; i < tmpvalues.length; i++) {
                if (3 <= i % 9 && i % 9 < 6)
                    values.push(tmpvalues[i])
            }

            gyrovalues = values;
            sceneVisualisation.init();
        },
        function (xhr) {

        },
        function (err) {
            console.log(err);
        }
    );
}
*/

function animateRealTime(x, y, z, part) {
    if (sceneVisualisation.modelbones[part]) {
        sceneVisualisation.modelbones[part].rotation.x = x;
        sceneVisualisation.modelbones[part].rotation.y = y;
        sceneVisualisation.modelbones[part].rotation.z = z;
        let mixerUpdateDelta = sceneVisualisation.clock.getDelta();

        if (sceneVisualisation.mixer != undefined) {
            sceneVisualisation.mixer.update(mixerUpdateDelta);
        }

        sceneVisualisation.stats.update();
        sceneVisualisation.renderer.render(sceneVisualisation.scene, sceneVisualisation.camera);
    }
}


function animate() {
    requestAnimationFrameId = requestAnimationFrame(animate);

    for (let part in motioniterators) {
        let sensordata = motioniterators[part].next();
        if (sensordata) {
            sceneVisualisation.modelbones[part].rotation.x = sensordata.x;
            sceneVisualisation.modelbones[part].rotation.z = sensordata.z;
            sceneVisualisation.modelbones[part].rotation.y = sensordata.y;
        }
    }

    let mixerUpdateDelta = sceneVisualisation.clock.getDelta();
    if (sceneVisualisation.mixer != undefined) {
        sceneVisualisation.mixer.update(mixerUpdateDelta);
    }
    sceneVisualisation.stats.update();
    sceneVisualisation.renderer.render(sceneVisualisation.scene, sceneVisualisation.camera);
}

function downloadmove() {

}

document.getElementById("downloadMove").addEventListener("click", function (event) {
    let uri = 'data:attachment/text,' + encodeURI((JSON.stringify(fullmotion.export())));
    let downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "moveData.json";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
});


function record() {
    if (recording) {
        document.getElementById("record").innerText = "Record";
        recording = false;
        motioniterators = fullmotion.getIterators();
        animate();
    } else {
        document.getElementById("record").innerText = "Stop";
        window.cancelAnimationFrame(requestAnimationFrameId);
        for (let part in motioniterators) {
            motioniterators[part].reset();
        }
        recording = true;
    }
}

document.getElementById("record").onclick = record;

var socket = io("http://192.168.43.211:3000");
sceneVisualisation.init();

let shouldermotion = new Motion();
let armmotion = new Motion();
let wristmotion = new Motion();
let swordmotion = new Motion();

let motions = {
    "mixamorigRightShoulder": shouldermotion,
    "mixamorigRightForeArm": armmotion,
    "mixamorigRightHand": wristmotion,
    "mixamorigRightHandRing2": swordmotion
};

let fullmotion = new FullMotion(motions);

//SOCKETS
socket.on('connect', onConnect);

function onConnect() {
    console.log('connect ' + socket.id);
    //animate();
}

socket.on('shoulder-imu', function (msg) {
    handleSensor(msg, "mixamorigRightShoulder")
});

socket.on('elbow-imu', function (msg) {
    handleSensor(msg, "mixamorigRightForeArm")
});

socket.on('wrist-imu', function (msg) {
    handleSensor(msg, "mixamorigRightHand")
});

socket.on('sword-imu', function (msg) {
    handleSensor(msg, "mixamorigRightHandRing2")
});


function handleSensor(msg, part) {
    if (typeof (msg) == "object" && msg.x !== undefined && msg.y !== undefined && msg.z !== undefined) {
        msg.x = parseFloat(msg.x);
        msg.y = parseFloat(msg.y);
        msg.z = parseFloat(msg.z);
        msg.date = parseInt(msg.date);
        //animateRealTime(msg.x, msg.y, msg.z, part);
        if (recording) {
            addData(msg.x, msg.y, msg.z, part, msg.date);
        }
    }
}

function addData(x, y, z, part, timestamp) {
    fullmotion.addDataToMotion(new SensorData(x, y, z, timestamp), part);
    //gyrovalues[][part] = {"x": x, "y": y, "z": z}
}

