export class Motion {

    constructor() {
        this.values = []; //array de sensordata
        this.start = -1;
    }

    add(sensordata) {
        this.values.push(sensordata);
/*
        if (this.start === -1) {
            this.start = sensordata.timestamp;
        }
        sensordata.timestamp = sensordata.timestamp - this.start;

        if (this.values.length === 0) {
            this.values.push(sensordata);
            return true;
        }

        //Peut être amélioré en O(log(n))
        for (let cpt = 0; cpt < this.values.length; cpt += 1) {
            if (sensordata.timestamp < this.values[cpt].timestamp) {
                this.values.splice(cpt, 0, sensordata);
                return true;
            }
        }

*/
    }

    getValues() {
        return this.values;
    }

    createIterator(){
        return new MotionIterator(this);
    }

}

export class MotionIterator{
    constructor(motion){
        this.cpt = 0;
        this.motion = motion
    }

    next(){
        this.cpt += 1;
        if(this.cpt >= this.motion.values.length)
            this.cpt = 0;

        if(this.motion.values[this.cpt] !== undefined)
            return this.motion.values[this.cpt];
        else
            return false;
    }

    reset(){
        this.cpt = 0;
    }
}

export class FullMotion{
    constructor(motions){
        this.motions = motions;
    }

    getIterators(){
        let iterators = {};
        for(let part in this.motions){
            iterators[part] = this.motions[part].createIterator();
        }
        return iterators;
    }

    addDataToMotion(sensordata, part){
        this.motions[part].add(sensordata);
    }

    export(){
        return JSON.stringify(this.motions);
    }

    import(JSONSave){
        this.motions = JSON.parse(JSONSave);
    }


}