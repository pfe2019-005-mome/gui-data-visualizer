
export class SensorData{

    constructor(x,y,z,timestamp){
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
    }

}